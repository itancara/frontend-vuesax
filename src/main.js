import Vue from 'vue'
import Vuesax from 'vuesax'
import App from './App.vue'
import 'vuesax/dist/vuesax.css'
import 'material-icons/iconfont/material-icons.css';
import Storage from './plugins/Storage'
import Service from './plugins/Service'

Vue.use(Storage);
Vue.use(Service);

Vue.use(Vuesax, {
  theme:{
    colors:{
      primary:'#0D47A1',
      success:'#1B5E20',
      danger:'#B71C1C',
      warning:'rgb(255, 130, 0)',
      dark:'rgb(36, 33, 69)'
    }
  }
});
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
